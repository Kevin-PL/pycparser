// #define INCLUDE_H
#define INCLUDE_H

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;
typedef signed   char  s8;
typedef signed   short u16;
typedef signed   int   s32;

#define MACRO_TEST
#define MACRO_TEST1 1
#define MACRO_TEST2

typedef struct
{
    u8 nOption0;
    u8 nOption1;
    u8 nOption2;
    u8 nOption3;
    u8 nOption4;
    u8 nOption5;
    u16 nThre0;
    u16 nThre1;
    u16 nThre2;
    u32 nParam0;
    u32 nParam1;
    u8 pReserved1[2];
    u8 pReserved[10];
    //this is the comment

}TControlTest;


