#include "general.h"
#include "ispcmd.h"
#include "hostcmd.h"

#define NULL 0u

void (*ISP_Cmd_Combine_Weight_cb)(TMiddle* pMiddle, TControlGeneral* pGeneral, TControlCombine *pControl, u8 * msg) = NULL;
void ISP_Cmd_Combine_Weight(TMiddle* pMiddle, TControlGeneral* pGeneral, TControlCombine *pControl, u8 * msg)
{
    if (ISP_Cmd_Combine_Weight_cb != NULL)
    {
        ISP_Cmd_Combine_Weight_cb(pMiddle, pGeneral, pControl, msg);
    }
    else
    {
        s32 i;
        u32 offset;
        u8* reg = msg;
        u8 camMode = (reg[0]&0xc0u)>>6;
        u8 bEnableTrafficMode;
        bEnableTrafficMode = (reg[0]&0x01u);
#if 0
        s32 tmp = 3;
        printf("this is for #if 0 comment test");
#ifdef TEST
        s32 nTest = 0;
        printf("this is for nesting #if test");
#else
        s32 nTest1 = 1;
        printf("this is for nesting #if test1");
#endif
#endif
        /* this is 
         * segment commnet
         * test */

        if(pControl->bDepressVSEnable>0u)
        {
            for(i=0; i<156; i++)
            {
                pMiddle->nWeight = reg[i+1]&0xffu;
                pMiddle->nRatio = (reg[i+156]&0xc0);
            }
        }
        else if(pControl->bDepressVSEnable==1u)
        {
            for(i=0; i<156; i++ )
            {
                pMiddle->nWeight = ((u32)reg[i+1]&0xffu)*(1- (u32)pMiddle->nRatio) + (u16)pControl->nWeight * pMiddle->nRatio;
                pMiddle->nRatio += pControl->nStep;
            }
        }
        else
        {
            pMiddle->nWeight = 1u;
            pMiddle->nRatio = 0u;
        }

        DBG_PRINTF(pGeneral.tCombine->nDebugMode, ((ISP_CMD_ID&0xffu)<<16u | 0x02u), "reg[0]:%s\n", reg[0]);
    }
}

s32 (*isp_host_cmd_cb)(u8 *msg, u32 hostID, s32 ret) = NULL;
s32 isp_host_cmd(u8 *msg, u32 hostID, s32 ret)
{
    if (isp_host_cmd_cb != NULL)
    {
        isp_host_cmd_cb
    }
    {
        u8 * reg = msg;
        u8 nTest;
        switch(hostID) {
            case ISP_CMD_COMBINE_WEIGHT:
                ISP_Cmd_Combine_Weight1(hostID, msg);
                break;
            case ISP_CMD_AWB_ROI:
                ISP_Cmd_AWB_ROI(msg);
                break;
            default:
                break;
        }
        return ret;
    }
}

s32 (*isp_host_cmd_cb)(u8 *msg, u32 hostID, s32 ret) = NULL;
s32 isp_host_cmd1(u8 *msg, u32 hostID, s32 ret)
{
    if (isp_host_cmd_cb != NULL)
    {
        isp_host_cmd_cb
    }
    {
        u8 * reg = msg;
        switch(hostID) {
            case ISP_CMD_COMBINE_WEIGHT:
                ISP_Cmd_Combine_Weight1(hostID, msg);
                break;
            case ISP_CMD_AWB_ROI:
                ISP_Cmd_AWB_ROI(msg);
                break;
            default:
                break;
        }
        return ret;
    }
}