// #define INCLUDE_H
#ifndef INCLUDE_H
#define INCLUDE_H

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;
typedef signed   char  s8;
typedef signed   short u16;
typedef signed   int   s32;

#define MACRO_TEST
#define MACRO_TEST1 1
#define MACRO_TEST2

typedef struct
{
    u8 nOption0;
    u8 nOption1;
    u8 nOption2;
    u8 nOption3;
    u8 nOption4;
    u8 nOption5;
    u16 nThre0;
    u16 nThre1;
    u16 nThre2;
#ifdef MACRO_TEST
    u32 nParam0;
    u32 nParam1;
#if MACRO_TEST1==0
    u8 nReserved1;
#ifdef MACRO_TEST2
    u8 nReserved2;
#endif
#elif MACRO_TEST1==1
    u8 pReserved1[2];
#endif
#else
    u16 nParam0;
    u16 nParam1;
#endif
    u8 pReserved[10];
#if 1
    //this is the comment
#endif

}TControlTest;

#endif

