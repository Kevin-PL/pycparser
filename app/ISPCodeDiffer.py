#-*- coding: utf-8 -*-
'''
@File       : ISPCodeDiffer.py
@Date       : 2020/9/19
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependency : Python 2.7 or Python 3+
@Description: diff isp source code
'''

import sys, os, re
import shutil

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))  # add ../lib to sys path
from lib import PyDiff

class CLine:
    def __init__(self, content, linenum):
        self.content = content
        self.linenum = linenum

class CFunctionPRS:
    def __init__(self, funcname):
        self.name = funcname
        self.clinelist = []  # list of CLine instance

class CModulePRS:
    def __init__(self, cfilepath):
        if not os.path.exists(cfilepath):
            ErrorInfo = "file '%s' does not exist" % (cfilepath)
            raise RuntimeError(ErrorInfo)
        self.filepath  = cfilepath
        self.name  = os.path.splitext(os.path.split(cfilepath)[1])[0]
        self.cfunclist = []

    def loadCFile(self):
        funcflag   = False  # function define match flag
        segcmtflag = False  # segment comment match flag
        ifcmtflag  = False  # #if 0 comment match flag
        bracecnt = lbracecnt = rbracecnt = 0
        ifmacrocnt = 0
        with open(self.filepath, 'r+') as fin:
            rawlines = fin.readlines()
            for linenum, line in enumerate(rawlines):
                cline = CLine(content=line, linenum=linenum+1)
                # #if 0 comment
                if re.match(r"^\s*#if\s+0.*&", line):  # match '#if 0' the begin of #if 0 comment
                    ifmacrocnt = 0
                    ifcmtflag = True
                if ifcmtflag == True:
                    if re.match(r'^\s*#if.*$'):
                        ifmacrocnt += 1
                    if re.match(r"^\s*#endif.*$"):
                        ifmacrocnt -= 1
                    if ifmacrocnt == 0:
                        ifcmtflag = False
                    if funcflag == True:
                        if "cfunc" not in vars():  # raise exception when cfunc does not exists
                            ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpected error"
                            raise RuntimeError(ErrorInfo)
                        cfunc.clinelist.append(cline)
                    continue
                # segment comment
                if re.match(r"^\s*/\*.*$", line):  # match '/*...' the begin of segment comment
                    segcmtflag = True
                if segcmtflag == True:
                    # print("match segment commnt", line, end='')
                    if re.match(r"^.*?\*/\s*$", line):  # match '...*/' the end of segment comment
                        segcmtflag = False
                    if funcflag == True:
                        if "cfunc" not in vars():  # raise exception when cfunc does not exists
                            ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpect error"
                            raise RuntimeError(ErrorInfo)
                        cfunc.clinelist.append(cline)
                    continue
                linenocmt, cmt = self._splitInlineComment(line)  # split in-line commnet to avoid in-line effect
                pattern_func = r"^(.+?)\s+(\w+)\s*\((.*?)\s*(\{\s*)?$"
                match_func = re.match(pattern_func, linenocmt)
                if match_func and funcflag==False:
                    funcflag = True
                    funcname = match_func.group(2)
                    cfunc = CFunctionPRS(funcname)
                    cfunc.clinelist.append(cline)
                    bracecnt  = 0
                    lbracecnt = list(linenocmt).count('{')
                    rbracecnt = list(linenocmt).count('}')
                    bracecnt += lbracecnt
                    bracecnt -= rbracecnt
                    continue
                if funcflag == True:
                    if "cfunc" not in vars():  # raise exception when cfunc does not exists
                        ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpect error"
                        raise RuntimeError(ErrorInfo)
                    cfunc.clinelist.append(cline)
                    lbracecnt = list(linenocmt).count('{')
                    rbracecnt = list(linenocmt).count('}')
                    bracecnt += lbracecnt
                    bracecnt -= rbracecnt
                    if bracecnt == 0:
                        self.cfunclist.append(cfunc)
                        funcflag = False
                    # continue

    def dump2file(self, destdir, extension='.txt'):
        if not os.path.exists(destdir):
            os.makedirs(destdir)
        funcnum = 0
        moduledir = os.path.join(destdir, self.name)
        if not os.path.exists(moduledir):
            os.mkdir(moduledir)
        print("=======================================================================")
        print("<module: %s>" % self.name)
        for cfunc in self.cfunclist:
            # print("<func: %s>" % cfunc.name)
            filename = cfunc.name + extension
            filepath = os.path.join(moduledir, filename)
            with open(filepath, 'w') as fout:
                for cline in cfunc.clinelist:
                    # print('[line %d] %s' % (cline.linenum, cline.content), end='')
                    fout.write(cline.content)
                print("> %s write done." % cfunc.name)
                funcnum += 1
        print("done, total %d funcitons" % funcnum)
            

    def _splitInlineComment(self, line):
        match_split = re.match(r"^(.*?)(//.*?)?$", line)
        if match_split.group(2):
            return (match_split.group(1), match_split.group(2))
        else:
            return (match_split.group(1), '')

DIFF_ADD = 0  # add
DIFF_RMV = 1  # remove
DIFF_MDF = 2  # modify
DIFF_UCG = 3  # unchanged

class CDiffItem:
    def __init__(self, name):
        self.name = name
        self.outcome = DIFF_UCG
        self.diff_result = None
        self.subitemlist = []

    def additem(self, item):
        self.subitemlist.append(item)
    
    def setoutcome(self, outcome):
        self.outcome = outcome
    
    def setresult(self, result):
        self.diff_result = result

class CDiff:
    def __init__(self, base, comp, tmpdir='.', dstdir='.'):
        self.base = base
        self.comp = comp
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)
        if not os.path.exists(dstdir):
            os.makedirs(dstdir)
        self.tmpdir = tmpdir
        self.dstdir = dstdir
        # attrib
        self.diffitemlist = []
        self.rmvlist = []
        self.addlist = []
        self.mdflist = []
        self.ucglist = []

    def compare(self):
        self._resolve()

    def _resolve(self):
        tmpbasedir = os.path.join(self.tmpdir, '_base_')
        tmpcompdir = os.path.join(self.tmpdir, '_comp_')
        # resolve base/comp to temp directory
        self._resolve_single(self.base, tmpbasedir)
        self._resolve_single(self.comp, tmpcompdir)
        # save diff item to list
        baselist = []
        complist = []
        for subdir in os.listdir(tmpbasedir):
            tmpsubdir = os.path.join(tmpbasedir, subdir)
            if not os.path.isdir(tmpsubdir):
                ErrorInfo = "unexpect parsed item '%s' under '%s'" % (subdir, tmpbasedir)
                raise RuntimeError(ErrorInfo)
            baselist.append(subdir)
        for subdir in os.listdir(tmpcompdir):
            tmpsubdir = os.path.join(tmpcompdir, subdir)
            if not os.path.isdir(tmpsubdir):
                ErrorInfo = "unexpect parsed item '%s' under '%s'" % (subdir, tmpcompdir)
                raise RuntimeError(ErrorInfo)
            complist.append(subdir)
        # compare diff items
        self.diffitemlist = self._preprocess_cmp(baselist, complist)
        for item in self.diffitemlist:
            # print(baselist, complist, item.name)
            destdir = os.path.join(self.dstdir, item.name)
            if item.name in complist:  # item in comp list
                if item.name in baselist:  # item in base list, in other words, 'modified' or 'unchanged'
                    tmpbaselist = self._listfiles(os.path.join(tmpbasedir, item.name))
                    tmpcomplist = self._listfiles(os.path.join(tmpcompdir, item.name))
                    diffsubitemlist = self._preprocess_cmp(tmpbaselist, tmpcomplist)
                    for subitem in diffsubitemlist:
                        if subitem.name in tmpcomplist:
                            if subitem.name in tmpbaselist:
                                fbasepath = os.path.join(tmpbasedir, '{}/{}'.format(item.name, subitem.name))
                                fcomppath = os.path.join(tmpcompdir, '{}/{}'.format(item.name, subitem.name))
                                sim = PyDiff.FileDiff.similarity(fbasepath, fcomppath)
                                if sim != 1.0:  # float value compare
                                    destfile = PyDiff.FileDiff.compare(fbasepath, fcomppath, destdir, destname=subitem.name+"_diff")
                                    subitem.diff_result = destfile
                                    subitem.outcome = DIFF_MDF
                                else:
                                    # destfile = PyDiff.FileDiff.compare(fbasepath, fcomppath, destdir, destname=subitem.name+"_diff")
                                    # subitem.diff_result = destfile
                                    subitem.outcome = DIFF_UCG
                            else:
                                fcomppath = os.path.join(tmpcompdir, '{}/{}'.format(item.name, subitem.name))
                                baselines = []
                                complines = PyDiff.FileDiff._readlines(fcomppath)
                                destfile = PyDiff.FileDiff.comparelines(baselines, complines, destdir, destname=subitem.name+"_diff")
                                subitem.diff_result = destfile
                                subitem.outcome = DIFF_ADD
                        else:
                            if subitem.name in tmpbaselist:
                                fbasepath = os.path.join(tmpbasedir, '{}/{}'.format(item.name, subitem.name))
                                baselines = PyDiff.FileDiff._readlines(fbasepath)
                                complines = []
                                destfile = PyDiff.FileDiff.comparelines(baselines, complines, destdir, destname=subitem.name+"_diff")
                                subitem.diff_result = destfile
                                subitem.outcome = DIFF_RMV
                                pass
                            else:
                                ErroInfo = "unexpected subitem '%s' for '%s' compare" % (subitem.name, item.name)
                                raise RuntimeError(ErrorInfo)
                    item.outcome = DIFF_MDF
                    item.subitemlist = diffsubitemlist
                else:  # item.name not in baselist
                    tmpbaselist = []
                    tmpcomplist = self._listfiles(os.path.join(tmpcompdir, item.name))
                    diffsubitemlist = self._preprocess_cmp(tmpbaselist, tmpcomplist)
                    for subitem in diffsubitemlist:
                        fcomppath = os.path.join(tmpcompdir, '{}/{}'.format(item.name, subitem.name))
                        baselines = []
                        complines = PyDiff.FileDiff._readlines(fcomppath)
                        destfile = PyDiff.FileDiff.comparelines(baselines, complines, destdir, destname=subitem.name+"_diff")
                        subitem.diff_result = destfile
                        subitem.outcome = DIFF_ADD
                    item.outcome = DIFF_ADD
                    item.subitemlist = diffsubitemlist
            else:  # item.name not in complist:
                if item.name in baselist:
                    tmpbaselist = self._listfiles(os.path.join(tmpbasedir, item.name))
                    tmpcomplist = []
                    diffsubitemlist = self._preprocess_cmp(tmpbaselist, tmpcomplist)
                    for subitem in diffsubitemlist:
                        fbasepath = os.path.join(tmpbasedir, '{}/{}'.format(item.name, subitem.name))
                        baselines = PyDiff.FileDiff._readlines(fbasepath)
                        complines = []
                        destfile = PyDiff.FileDiff.comparelines(baselines, complines, destdir, destname=subitem.name+"_diff")
                        subitem.diff_result = destfile
                        subitem.outcome = DIFF_RMV
                    item.outcome = DIFF_RMV
                    item.subitemlist = diffsubitemlist
                else:
                    ErrorInfo = "unexpect item '%s'" % item.name
                    raise RuntimeError(ErrorInfo)
        ## summarize
        for item in self.diffitemlist:
            if item.outcome == DIFF_RMV:
                self.rmvlist.append(item)
            elif item.outcome == DIFF_ADD:
                self.addlist.append(item)
            elif item.outcome == DIFF_MDF:
                self.mdflist.append(item)
            else:
                self.ucglist.append(item)
        ## print 
        print("==========================================================")
        print("diff result '%s' <==> '%s'" % (self.base, self.comp))
        indent = ' '*4
        print("<unchanged list>")
        for item in self.ucglist:
            print("%smodule name: %s" % (indent, item.name))
        print("<removed list>")
        for item in self.rmvlist:
            print("%smodule name: %s" % (indent, item.name))
        print("<added list>")
        for item in self.addlist:
            print("%smodule name: %s" % (indent, item.name))
        print("<modified list>")
        for item in self.mdflist:
            print("%smodule name: %s" % (indent, item.name))
            tmprmvlist = []
            tmpaddlist = []
            tmpmdflist = []
            tmpucglist = []
            for subitem in item.subitemlist:
                if subitem.outcome == DIFF_RMV:
                    tmprmvlist.append(subitem)
                elif subitem.outcome == DIFF_ADD:
                    tmpaddlist.append(subitem)
                elif subitem.outcome == DIFF_MDF:
                    tmpmdflist.append(subitem)
                else:
                    tmpucglist.append(subitem)
            print("%s<sub unchanged list>" % indent)
            for subitem in tmpucglist:
                print("%ssub-module: %s" % (indent*2, subitem.name))
            print("%s<sub removed list>" % indent)
            for subitem in tmprmvlist:
                print("%ssub-module: %s" % (indent*2, subitem.name))
            print("%s<sub added list>" % indent)
            for subitem in tmpaddlist:
                print("%ssub-module: %s" % (indent*2, subitem.name))
            print("%s<sub modified list>" % indent)
            for subitem in tmpmdflist:
                print("%ssub-module: %s  diff-result: %s" % (indent*2, subitem.name, subitem.diff_result))

    def _listfiles(self, tmpdir):
        rlist = []
        for fname in os.listdir(tmpdir):
            fpath = os.path.join(tmpdir, fname)
            if os.path.isfile(fpath):
                rlist.append(fname)
        return rlist

    def _preprocess_cmp(self, baselist, complist):
        unionlist = complist + [i for i in baselist if i not in complist]
        return [CDiffItem(i) for i in unionlist]

    def _resolve_single(self, src, tmpdir):
        CDiff._validate_dir(tmpdir)
        CDiff._clean_dir(tmpdir)
        if os.path.isfile(src):
            m = CModulePRS(src)
            m.loadCFile()
            m.dump2file(tmpdir)
        if os.path.isdir(src):
            for fname in os.listdir(src):
                if os.path.isdir(os.path.join(src, fname)):
                    continue
                tmpfilepath = os.path.join(src, fname)
                m = CModulePRS(tmpfilepath)
                m.loadCFile()
                m.dump2file(tmpdir)

    @staticmethod
    def _clean_dir(tmpdir):
        dirlist  = []
        filelist = []
        for root,dirs,files in os.walk(tmpdir):
            for item in dirs:
                dirlist.append(os.path.join(root, item))
                print(os.path.join(root, item))
            for item in files:
                filelist.append(os.path.join(root, item))
                print(os.path.join(root, item))
        for tmpfile in filelist:
            os.remove(tmpfile)
        for tmpdir in dirlist:
            os.rmdir(tmpdir)

    @staticmethod
    def _copy_dir(srcdir, dstdir):
        if not os.path.exists(dstdir):
            os.makedirs(dstdir)
        srcfilelist = []
        dstfilelist = []
        for root,dirs,files in os.walk(srcdir):
            relative = root.split(srcdir)[1][1:]
            # print(relative)
            if len(relative)>0 and relative[0] == '.':  # bypass hiden path
                continue
            # recurse subdirs
            for item in dirs:
                tmpdir = os.path.realpath(os.path.join(dstdir,relative, item))  # dest directory
                if not os.path.exists(tmpdir):
                    os.makedirs(tmpdir)
                    pass
            for item in files:
                srcfile = os.path.realpath(os.path.join(root, item))
                dstfile = os.path.realpath(os.path.join(dstdir, relative, item))
                srcfilelist.append(srcfile)
                dstfilelist.append(dstfile)
            
        for src,dst in zip(srcfilelist,dstfilelist):
            print("cp '%s' -> '%s'" % (src, dst))
            shutil.copy(src, dst)
    
    @staticmethod
    def _validate_dir(tmpdir):
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)

if __name__ == "__main__":
    curdir  = os.path.dirname(os.path.realpath(__file__))
    rootdir = os.path.realpath(os.path.join(curdir, '..'))
    # ''' test copy dir
    srcdir = os.path.realpath(os.path.join(rootdir, '../PyCParser/share'))
    dstdir = os.path.join(rootdir, 'testcp')
    CDiff._copy_dir(srcdir, dstdir)
    # '''

    ''' test compare
    # print(curdir)
    cfilepath = os.path.join(rootdir, 'test/test.c')
    # cfilepath = os.path.join(curdir, '../test/aecagc_ram.c')
    m = CModulePRS(cfilepath)
    # line = "  pMiddle->nWeight = reg[i+1]&0xffu; //this inline comment"
    # line = "  pMiddle->nWeight = reg[i+1]&0xffu; //this inline comment"
    # line = "  pMiddle->nWeight = reg[i+1]&0xffu; //this inline comment"
    # print(m._splitInlineComment(line))
    m.loadCFile()
    # for cfunc in m.cfunclist:
    #     print("<func: %s>" % cfunc.name)
    #     for cline in cfunc.clinelist:
    #         print('[line %d] %s' % (cline.linenum, cline.content), end='')
    destdir = os.path.join(rootdir, 'test')
    m.dump2file(destdir)

    filebase = os.path.join(rootdir, 'test/test_base')
    filecomp = os.path.join(rootdir, 'test/test_comp')
    tmpdir   = os.path.join(rootdir, 'tmp')
    dstdir   = os.path.join(rootdir, 'tmp/test_dst')
    d = CDiff(filebase, filecomp, tmpdir, dstdir)
    # d._resolve()
    d.compare()
    # CDiff._clean_dir(os.path.join(tmpdir, '_base_'))
    # '''

