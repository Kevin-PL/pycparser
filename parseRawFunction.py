#-*- coding: utf-8 -*-
'''
@File       : parseRawCFunciton.py
@Date       : 2020/9/4
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependecy  : Python 2.7 or Python 3+
@Description: parse raw function from c file
'''

import sys, os, re

class CLine:
    def __init__(self, content, linenum):
        self.content = content
        self.linenum = linenum

class CFunctionPRS:
    def __init__(self, funcname):
        self.name = funcname
        self.clinelist = []  # list of CLine instance

class CModulePRS:
    def __init__(self, cfilepath):
        if not os.path.exists(cfilepath):
            ErrorInfo = "file '%s' does not exist" % (cfilepath)
            raise RuntimeError(ErrorInfo)
        self.filepath  = cfilepath
        self.cfunclist = []

    def loadCFile(self):
        funcflag   = False  # function define match flag
        segcmtflag = False  # segment comment match flag
        ifcmtflag  = False  # #if 0 comment match flag
        bracecnt = lbracecnt = rbracecnt = 0
        ifmacrocnt = 0
        with open(self.filepath, 'r+') as fin:
            rawlines = fin.readlines()
            for linenum, line in enumerate(rawlines):
                cline = CLine(content=line, linenum=linenum+1)
                # #if 0 comment
                if re.match(r"^\s*#if\s+0.*&", line):  # match '#if 0' the begin of #if 0 comment
                    ifmacrocnt = 0
                    ifcmtflag = True
                if ifcmtflag == True:
                    if re.match(r'^\s*#if.*$'):
                        ifmacrocnt += 1
                    if re.match(r"^\s*#endif.*$"):
                        ifmacrocnt -= 1
                    if ifmacrocnt == 0:
                        ifcmtflag = False
                    if "cfunc" not in vars():  # raise exception when cfunc does not exists
                        ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpect error"
                        raise RuntimeError(ErrorInfo)
                    if funcflag == True:
                        cfunc.clinelist.append(cline)
                    continue
                # segment comment
                if re.match(r"^\s*/\*.*$", line):  # match '/*...' the begin of segment comment
                    segcmtflag = True
                if segcmtflag == True:
                    # print("match segment commnt", line, end='')
                    if re.match(r"^.*?\*/\s*$", line):  # match '...*/' the end of segment comment
                        segcmtflag = False
                    if "cfunc" not in vars():  # raise exception when cfunc does not exists
                        ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpect error"
                        raise RuntimeError(ErrorInfo)
                    if funcflag == True:
                        cfunc.clinelist.append(cline)
                    continue
                linenocmt, cmt = self._splitInlineComment(line)  # split in-line commnet to avoid in-line effect
                pattern_func = r"^(.+?)\s+(\w+)\s*\((.*?)\s*(\{\s*)?$"
                match_func = re.match(pattern_func, linenocmt)
                if match_func and funcflag==False:
                    funcflag = True
                    funcname = match_func.group(2)
                    cfunc = CFunctionPRS(funcname)
                    cfunc.clinelist.append(cline)
                    bracecnt  = 0
                    lbracecnt = list(linenocmt).count('{')
                    rbracecnt = list(linenocmt).count('}')
                    bracecnt += lbracecnt
                    bracecnt -= rbracecnt
                    continue
                if funcflag == True:
                    if "cfunc" not in vars():  # raise exception when cfunc does not exists
                        ErrorInfo = "cfunction instance not exists, maybe resulting from other unexpect error"
                        raise RuntimeError(ErrorInfo)
                    cfunc.clinelist.append(cline)
                    lbracecnt = list(linenocmt).count('{')
                    rbracecnt = list(linenocmt).count('}')
                    bracecnt += lbracecnt
                    bracecnt -= rbracecnt
                    if bracecnt == 0:
                        self.cfunclist.append(cfunc)
                        funcflag = False
                    # continue

    def _splitInlineComment(self, line):
        match_split = re.match(r"^(.*?)(//.*?)?$", line)
        if match_split.group(2):
            return (match_split.group(1), match_split.group(2))
        else:
            return (match_split.group(1), '')

def _rmRightBlank(line):
    pass

if __name__ == "__main__":
    curdir = os.path.realpath(os.path.dirname(__file__))
    # print(curdir)
    cfilepath = os.path.join(curdir, 'test/test.c')
    m = CModulePRS(cfilepath)
    # line = "  pMiddle->nWeight = reg[i+1]&0xffu; //this inline comment"
    # print(m._splitInlineComment(line))
    m.loadCFile()
    for cfunc in m.cfunclist:
        print("<func: %s>" % cfunc.name)
        for cline in cfunc.clinelist:
            print('[line %d] %s' % (cline.linenum, cline.content), end='')