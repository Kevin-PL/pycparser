#-*- coding: utf-8 -*-
'''
@File       : PyCProcessor.py
@Date       : 2020/10/2
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependency : Python 2.7 or Python 3+
@Description: C preprocess class structure
'''

import sys, os, re
import copy

try:
    import PyCBasic
except ImportError:
    from lib import PyCBasic

class CProcessor(PyCBasic.CBasic):
    def __init__(self, rawlines, filepath, includedir, logmask=False):
        super().__init__()
        # input param
        self.rawlines = rawlines
        self.filepath = filepath
        self.filename = os.path.split(filepath)[1]
        self.includedir = includedir
        self.logmask = logmask
        # parsed attrib
        self.includelist = []
        self.includelist_valid = []
        self.objmacro_dict = {}
        self.funcmacro_dict = {}


    def IntraMacroExpand(self, nlines, objmacro_dict, funcmacro_dict):
        '''intra macro expand
        such as: expand 'nLDRMax=_INPUT_BITS_*2048' as 'nLDRMax=10*2048' supposed that '#define _INPUT_BITS_ 10'
        
        Args:
            nlines: line list with linenum, [[line_num, line_content], ...]
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}
            funcmacro_dict: function-like macro dictionary (current not used), {macro:[macro_param,macro_def], ...}

        Returns:
            return the line list with linenum which in-line macro has been expanded
        '''
        rnlines = []
        segcmtflag = False
        rawnlines = copy.deepcopy(nlines)
        for nline in rawnlines:
            linenum, line = nline
            # print("<debug intra expand> [line %d] %s" % (linenum, line), end='')
            # bypass comment
            if re.match(r"^\s*\/\/.*", line):  # in-line comment, such as '//comment'
                rnlines.append([linenum, line])
                continue
            if re.match(r"^\s*\/\*.*", line):  # inter-line comment, such as '/*...*/ '
                segcmtflag = True
            if segcmtflag == True:
                rnlines.append([linenum, line])
                if re.match(r".*\*\/\s*$", line):
                    segcmtflag = False
                continue
            # avoid replace macro definition or condition macro
            if re.match(r"^\s*#.+$", line):
                rnlines.append([linenum, line])
                continue
            # expand in-line intra object-like macro
            nline_expd = self._lineMacroExpand(nline, objmacro_dict, funcmacro_dict)
            rnlines.append(nline_expd)
        # double check segment comment flag
        if segcmtflag == True:
            ErrorInfo = "syntax error segment comment not coupled"
            raise RuntimeError(ErrorInfo)
        return rnlines

    def _lineMacroExpand(self, nline, objmacro_dict, funcmacro_dict):
        '''expand object-like macro of given line
        
        Args:
            nline: line with linenum, [linenum, line]
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}
            funcmacro_dict: function-like macro dictionary (current not used), {macro:[macro_param,macro_def], ...}

        Returns:
            return expanded line with its linenum

        Limit:
            only support object-like macro expand
        '''
        # linenum, line = copy.deepcopy(nline)
        linenum, line = nline
        line_ncmt, blank, comment = self._splitPostComment(line, splitblank=True)  # split in-line comment
        # recursely expand in-line object-like macro
        line_expd = self._objmacroExpand(line_ncmt, objmacro_dict)
        return [linenum, line_expd+blank+comment]

    def _objmacroExpand(self, expression, objmacro_dict):
        '''expand intra macro within given expression

        Args:
            expression: input c expression without in-line comment
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}

        Returns:
            return expanded expression
        '''
        expression_expd = ''.join([objmacro_dict[i] if i in objmacro_dict else i for i in re.split(r'(\w+)', expression)])
        while self._isContainMacro(expression_expd, objmacro_dict):
            expression_expd = ''.join([objmacro_dict[i] if i in objmacro_dict else i for i in re.split(r'(\w+)', expression_expd)])
        return expression_expd

    def _objmacroSelfExpand(self, objmacro_dict):
        '''expand object-like macro within object-like macro definition

        Args:
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}

        Returns:
            return expanded object-like macro dictionary
        '''
        objmacro_bpdict = copy.deepcopy(objmacro_dict)
        for objmacro in objmacro_dict:
            definition = objmacro_dict[objmacro]
            definition = self._objmacroExpand(definition, objmacro_bpdict)
            objmacro_dict[objmacro] = definition

    def _funcmacroExpand(self, expression, funcmacro_dict):
        '''expand function-like macro within given expression

        Args:
            expression: input c expression without in-line comment
            funcmacro_dict: function-like macro dictionary

        Returns:
            return expanded expression

        Limits:
            not implement
        '''
        pass

    def _isContainMacro(self, line_ncmt, macro_dict):
        '''check input line (without in-line comment) whether contain macro

        Args:
            line_ncmnt: line without in-line comment
            macro_dict: macro define dictionary, {macro:xxx, ...}

        Returns:
            return the check result (True or False)
        '''
        tmplist = re.split(r'(\w+)', line_ncmt)
        for item in tmplist:
            if item in macro_dict.keys():
                return True
        return False

    def IncludeParse(self, nlines, includedir):
        '''parse include header of given lines
        
        Args:
            nlines: line list with linenum
            includedir: include directory

        Returns:
            includelist: included header (name) list
            includelist_valid: header path list of which validity are checked 
                               according to given include directory
            implict return parsed included header list / valid included headerpath list
        '''
        includelist = []
        includelist_valid = []
        for nline in nlines:
            linenum, line = nline
            self._includeMatch(nline, includelist)
        # check validity header of parsed include list
        if not includedir or not os.path.exists(includedir):
            include = os.path.dirname(os.path.realpath(self.filepath))
        else:
            include = includedir
        for header in includelist:
            headerpath = os.path.join(include, header)
            if os.path.exists(headerpath):
                includelist_valid.append(os.path.realpath(headerpath))
            else:
                WarningInfo = "included header '%s' in '%s' not found" % (header, self.filename)
                self.debug_print(WarningInfo)
        return [includelist, includelist_valid]

    def _includeMatch(self, nline, includelist):
        '''include statement match

        Args:
            nline: line with linenum, [linenum, line]
            includelist: included header (name) list

        Returns:
            return check result True/False and update input include list
        '''
        linenum, line = nline
        line_ncmt, comment = self._splitPostComment(line)  # split in-line comment
        # check and parse include
        pattern_include = r'^\s*#\s*include\s+(?:<|")(.+?)(?:>|")\s*$'
        match_include = re.match(pattern_include, line_ncmt)
        if match_include:
            header = self._rmBoundaryBlank(match_include.group(1))
            if header not in includelist:
                includelist.append(header)
            else:
                # ErrorInfo = "[Line %d] repeatly include \'%s\' header file in \'%s\'." % (linenum, include, self.filename)
                # raise RuntimeError(ErrorInfo)
                WarningInfo = "[Line %d] repeatly include \'%s\' header file in \'%s\'." % (linenum, header, self.filename)
                self.debug_print(WarningInfo)
            return True
        return False

    def MacroParse(self, nlines):
        '''parse macro definitions of given lines

        Args:
            nlines: line list with linenum

        Returns:
            objmacro_dict: object-like macro dictionary
            funcmacro_dict: function-like macro dictionary
            return parsed macro definition dictionary
        '''
        objmacro_dict = {}
        funcmacro_dict = {}
        for nline in nlines:
            self._macroDefineMatch(nline, objmacro_dict, funcmacro_dict)
        return [objmacro_dict, funcmacro_dict]

    def _macroDefineMatch(self, nline, objmacro_dict, funcmacro_dict):
        '''check and parse macro define statement

        Args:
            nline: line with linenum
            objmacro_dict: object-like macro dictionary, both input and output
            funcmacro_dict: function-like macro dictionary, both input and output
        
        Returns:
            return check result True/False and parsed macro definition dictionary
        '''
        linenum, line = nline
        line_ncmt, comment = self._splitPostComment(line)  # split in-line comment
        # macro definition parse
        pattern_define = r"^\s*#\s*define\s+(.+)$"
        match_define = re.match(pattern_define, line_ncmt)
        if match_define:
            tmpMacroDef = self._rmBoundaryBlank(match_define.group(1))
            pattern_funcMacro = r"^\s*(\w+)\((.+?)\)\s+(.*)$"
            match_funcMacro = re.match(pattern_funcMacro, tmpMacroDef)
            pattern_objMacro = r"^\s*(\w+)\s*(.*)$"
            match_objMacro = re.match(pattern_objMacro, tmpMacroDef, re.S)
            if match_funcMacro:  # pattern_funcMacro = r"^\s*(\w+)\s*\((.+?)\)\s+(.*)$"
                funcMacro = match_funcMacro.group(1)
                funcMacroParam = match_funcMacro.group(2)
                funcMacroBody = self._rmBoundaryBlank(match_funcMacro.group(3))
                if not funcMacro.isupper():
                    WarningInfo = "[Line %d] Warning: macro \'%s\' is not capital format defined in \'%s\'." % (linenum, funcMacro, self.filename)
                    self.debug_print(WarningInfo)
                # print("<DEBUG define func> name:%s param:%s body:%s" % (funcMacro, funcMacroParam, funcMacroBody))
                if funcMacro not in funcmacro_dict.keys():
                    funcmacro_dict[funcMacro] = [funcMacroParam, funcMacroBody]
            elif match_objMacro:  # pattern_objMacro = r"^\s*(\w+)\s+(.*)$"
                objMacro = match_objMacro.group(1)
                objMacroBody = self._rmBoundaryBlank(match_objMacro.group(2))
                if not objMacro.isupper():
                    WarningInfo = "[Line %d] Warning: macro \'%s\' is not capital format defined in \'%s\'." % (linenum, objMacro, self.filepath)
                    self.debug_print(WarningInfo)
                # print("<DEBUG define obj> name:%s body:%s" % (objMacro, objMacroBody))
                if objMacro not in objmacro_dict.keys():
                    objmacro_dict[objMacro] = objMacroBody
            else:
                ErrorInfo = "[Line %d] Invalid define syntax: %s in \'%s\'." % (linenum, repr(line), self.filename)
                raise RuntimeError(ErrorInfo)
            # print("<DEBUG macro>", objmacro_dict)
            return True
        return False

    def RecurseMacroParse(self, filepath, includedir, selfcontain=True, linebreakconnect=True):
        '''recure parse macro definition across included header (valid) list

        Args:
            nlines: line list with linenum
            objmacro_dict: object-like dictionary
            funcmacro_dict: function-like dictionary
            includelist_valid: included valid header path list

        Returns:
            return recurse parsed macro definition dictionary
        '''
        objmacro_dict = {}
        funcmacro_dict = {}
        stack = []
        parsed_set = set()
        # parse root file
        fin = open(filepath, 'r')
        rawnlines = [[index+1, line] for index,line in enumerate(fin.readlines())]
        if linebreakconnect:
            rawnlines = self.LineBreakConnect(rawnlines)
        fin.close()
        rootObjMacroDict, rootFuncMacroDict = self.MacroParse(rawnlines)
        rootIncludeList, rootIncludeList_valid = self.IncludeParse(rawnlines, includedir)
        # update macro dictionary
        if selfcontain:
            objmacro_dict.update(rootObjMacroDict)
            funcmacro_dict.update(rootFuncMacroDict)
        # update parsed_set
        parsed_set.add(os.path.realpath(filepath))
        # update stack
        stack.extend(rootIncludeList_valid)
        while len(stack) > 0:
            tmpheaderpath = stack.pop(0)
            if tmpheaderpath not in parsed_set:
                print("<debug>", tmpheaderpath)
                fin = open(tmpheaderpath, 'r')
                rawnlines = [[index+1, line] for index,line in enumerate(fin.readlines())]
                if linebreakconnect:
                    rawnlines = self.LineBreakConnect(rawnlines)
                fin.close()
                tmpObjMacroDict, tmpFuncMacroDict = self.MacroParse(rawnlines)
                tmpIncludeList, tmpIncludeList_valid = self.IncludeParse(rawnlines, includedir)
                # update macro dictionary
                objmacro_dict.update(tmpObjMacroDict)
                funcmacro_dict.update(tmpFuncMacroDict)
                # update parsed set
                parsed_set.add(tmpheaderpath)
                # update stack
                stack = tmpIncludeList_valid + stack
        return [objmacro_dict, funcmacro_dict]

    def _preCrossline(self, nlines):
        '''convert IMPLICIT cross line to EXPLICIT cross line
        
        Args:
            nlines: line list with linenum

        Returns:
            nlines_ret: implict-to-explict converted line list
        '''
        nlines_ret = []
        segcmtflag = False
        typedeflag = False
        bracecnt = lbracecnt = rbracecnt = 0
        linenum = 0
        for nline in nlines:
            linenum, line = nline
            # bypass comment
            if re.match(r"^\s*\/\/.*", line):  # in-line comment, such as '//xxx'
                nlines_ret.append(nline)
                continue
            if re.match(r"^\s*\/\*.*", line):  # inter-line comment, such as '/*...*/'
                segcmtflag = True
                # print("<DEBUG comment s>",linenum,line)
            if segcmtflag == True:
                nlines_ret.append(nline)
                if re.match(r".*?\*\/\s*$", line):
                    segcmtflag = False
                    # print("<DEBUG comment e>", linenum, line)
                continue
            # bypass specific case
            if re.match(r'^\s*$', line):  # empty line
                nlines_ret.append(nline)
                continue
            if re.match(r'^\s*#.+$', line):  # begin with '#': macro
                nlines_ret.append(nline)
                continue
            if re.match(r'^.*?\\\s+$', line):  # end with '\': explict crossline
                nlines_ret.append(nline)
                continue
            # condition expression
            if re.match(r'\s*(\{\s*)*(if|else|while|for|switch|do).*$', line):
                nlines_ret.append(nline)
                continue
            # split in-line comment from expression
            line_ncmt, comment = self._splitPostComment(line)
            line_ncmt = self._rmBoundaryBlankR(line_ncmt)  # remove blank char at boundary
            # line_ncmt = self._mergeBlank(line_ncmt)  # merge mulit-blanks to one-blank
            # typedef struct / enum
            if re.match(r'^\s*(typedef\s+)?(struct|enum).+$', line):
                typedeflag = True
                bracecnt = 0
                lbracecnt = list(line_ncmt).count('{')
                rbracecnt = list(line_ncmt).count('}')
                bracecnt += lbracecnt
                bracecnt -= rbracecnt
                nlines_ret.append(nline)
                # print("<debug precrossl> [line %d] %s" % (linenum, nline[1]), end='')
                continue
            if typedeflag == True:
                lbracecnt = list(line_ncmt).count('{')
                rbracecnt = list(line_ncmt).count('}')
                bracecnt += lbracecnt
                bracecnt -= rbracecnt
                if bracecnt == 0:
                    typedeflag = False
                nlines_ret.append(nline)
                # print("<debug precrossl> [line %d] %s" % (linenum, nline[1]), end='')
                continue
            # end with { or }
            if re.search(r'(\{|\})\s*$', line):
                if re.match(r'^.*?=\s*\{\s*$', line):  # cover case like "structA a = {"
                    ending = re.match(r"^.*?(\s+)$", line).group(1)
                    nlines_ret.append([linenum, self._rmBoundaryBlankR(line)+'\\'+ending])
                else:
                    nlines_ret.append(nline)
                # nlines_ret.append(nline)
                continue
            # function definition
            if re.match(r"^(.+?)\s+(\w+)\s*\((.*?)\)\s*(\{\s*)?$", line):
                nlines_ret.append(nline)
                continue
            # normal expression
            if re.match(r"^.*?;\s*$", line):
                nlines_ret.append(nline)
                continue
            
            # norm expression with in-line comment, like 'u32 roiweight[6]={0u,0u,0u,0u,0u,0u}; ///roiweight [3] is win 16x16'
            if re.match(r"^.*?;\s*$", line_ncmt):
                nlines_ret.append(nline)
                continue
            # single '{' line
            if re.match(r"^\s*(?:\{|\})\s*$", line_ncmt):
                nlines_ret.append(nline)
                continue
            # all above not matched, indicate that it is an IMPLICIT cross line, solution: add '\' to turn it to EXPLICIT cross line
            nlines_ret.append([linenum, line_ncmt + ' \\\n'])
        # double check segment comment / typedef flag
        if segcmtflag or typedeflag:
            print(line, segcmtflag, typedeflag)
            ErrorInfo = '[Line: %d] Error erupted in pre-connect crossline.' % linenum
            raise RuntimeError(ErrorInfo)
        return nlines_ret

    def _connectCrossline(self, nlines):
        '''connect EXPLICIT cross line end with '\'
        
        Args:
            nlines: line list with linenum

        Returns:
            nlines_ret: connected line list
        '''
        lines_ret = []
        segcmtflag = False
        crosslineFlag = False
        # print('<DEBUG> total lines: %d' % len(rawLines))
        for nline in nlines:
            linenum, line = nline
            # print("<DEBUG>", index, line)
            # bypass comment
            if re.match(r"^\s*\/\/.*", line):  # single line comment '//...'
                lines_ret.append(nline)
                continue
            if re.match(r"^\s*\/\*.*", line):  # inter-line comment '/*...*/'
                segcmtflag = True
            if segcmtflag == True:
                lines_ret.append(nline)
                if re.match(r".*?\*\/\s*$", line):
                    segcmtflag = False
                continue
            # in-line comment to avoid case like 'int a; //this comment  \'
            if re.match(r"^(.+?)(?:\/{2,}|\/\*)(.*)$", line):
                if crosslineFlag == False:
                    lines_ret.append(nline)
                else:  # within cross line
                    # [hint] re.match(r'^(.*?)\s*\\\s*$', tmpline).group(1) is the previous line within pre-cossline list
                    tmpline = re.match(r'^(.*?)\s*\\\s*$', tmpline).group(1) + line
                    lines_ret.append([tmplinenum, tmpline])
                crosslineFlag = False
                continue
            # end with explict crossline symbol '\'
            if re.match(r'.*?\\\s+$', line):
                if crosslineFlag == False:
                    crosslineFlag = True
                    tmplinenum = linenum
                    tmpline = line
                else:
                    tmpline = re.match(r'^(.*?)\s*\\\s*$', tmpline).group(1) + line
                    crosslineFlag = True
            else:
                if crosslineFlag == False:
                    lines_ret.append(nline)
                else:
                    tmpline = re.match(r'^(.*?)\s*\\\s*$', tmpline).group(1) + line
                    lines_ret.append([tmplinenum, tmpline])
                crosslineFlag = False
        # double check flag
        if crosslineFlag or segcmtflag:
            ErrorInfo = "syntax error in \'%s\'" % self._simplifyPath(self.filepath)
            raise RuntimeError(ErrorInfo)
        return lines_ret

    def LineBreakConnect(self, nlines):
        '''connect line-break
         
         Args:
            nlines: line list with linenum

        Returns:
            nlines_ret: connected line list
        '''
        nlines_ret = []
        rawnlines  = copy.deepcopy(nlines)
        rawnlines  = self._preCrossline(rawnlines)
        nlines_ret = self._connectCrossline(rawnlines)
        return nlines_ret

    def LineFilter(self, nlines, filterout=None):
        '''filter out specific lines

        Args:
            nlines: line list with linenum
            filterout: filter-out function
        
        Returns:
            return filtered lines
        '''
        rnlist = []
        for nline in nlines:
            linenum, line = nline
            if callable(filterout):
                newline = line if not filterout(line) else (self._getBoundaryBlankL(line)+'// Removed by filter-out function: '+self._rmBoundaryBlankL(line))
                rnlist.append([linenum, newline])
            else:
                rnlist.append(nline)
        return rnlist

    def FilterOutRef(self, rawline):
        '''reference filter-out function, filter 'DBG_PRINTF, DPC' out

        Args:
            rawline: input line content

        Returns:
            return check result, matched then return true else return false
        '''
        filter_pattern = r"^\s*(DBG_PRINTF|DBG_DPC).*$"
        return True if re.match(filter_pattern, rawline) else False

    def _macroConditionInterpret(self, condition, objmacro_dict, funcmacro_dict={}):
        '''interpret macro condition
        
        Args:
            condition: parsed macro condition
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}
            funcmacro_dict: function-like macro dictionary (current not used), {macro:[macro_param,macro_def], ...}

        Returns:
            return interpreted macro condition result

        Limits:
            only support object-like macro expand / interpert
        '''
        newcondition = self._objmacroExpand(condition, objmacro_dict)
        # [TODO] add function-like macro expand
        newcondition = self._simplify(newcondition)
        return bool(eval(newcondition))

    def ConditionMacroExpand(self, nlines, objmacro_dict, funcmacro_dict):
        '''expand conditional macro

        Args:
            nlines: line list with linenum
            objmacro_dict: object-like macro dictionary, {macro:macro_def, ...}
            funcmacro_dict: function-like macro dictionary (current not used), {macro:[macro_param,macro_def], ...}
        
        Returns:
            return expanded line list
        '''
        rawnlines = copy.deepcopy(nlines)
        rnlines = []  # return nlines
        matchFlagStack = []
        for nline in rawnlines:
            linenum, rawline = nline
            line = self._rmBoundaryBlank(rawline)
            # match '#if'
            if re.match(r"^\s*#if.*$", line):
                # print("<debug #if> stack:", matchFlagStack, line, end=' => ')
                if matchFlagStack and matchFlagStack[-1] == 0:
                    matchFlagStack.append(0)
                    # print(matchFlagStack)
                    continue
                # condition interpret
                match_if = re.match(r"^\s*#if\s+(.*?)\s*$", line)
                match_ifdef = re.match(r"^\s*#ifdef\s+(\w+).*$", line)
                match_ifndef = re.match(r"^\s*#ifndef\s+(\w+).*$", line)
                if match_if:  # match '#if xxx'
                    condition = match_if.group(1)
                    condition = self._rmBoundaryBlank(condition)
                    condition = self._splitPostComment(condition)[0]  # remove possible post comment, case like '#if 0  //this is comment'
                    cond = self._macroConditionInterpret(condition, objmacro_dict, funcmacro_dict)
                elif match_ifdef:  # match '#ifdef xxx'
                    condition = match_ifdef.group(1)
                    condition = self._splitPostComment(condition)[0]
                    cond = condition in objmacro_dict
                elif match_ifndef:  # match '#ifndef xxx'
                    condition = match_ifndef.group(1)
                    condition = self._splitPostComment(condition)[0]
                    cond = condition not in objmacro_dict
                else:
                    ErrorInfo = "unvalid #if definition '%s'" % line
                    raise RuntimeError(ErrorInfo)
                # process according to interpreted result
                if cond:
                    matchFlagStack.append(1)
                    # if not matchFlagStack:  # empty stack
                    #     matchFlagStack.append(1)
                    # # avoid case of pre-condition false but cur-condition true
                    # elif matchFlagStack[-1] == 1:  # pre-condition true
                    #     matchFlagStack.append(1)
                    # elif matchFlagStack[-1] == 0:  # pre-condition false
                    #     matchFlagStack.append(0)
                    # else:
                    #     pass
                else:
                    matchFlagStack.append(0)
                # print(matchFlagStack)
                continue
            # match '#elif'
            elif re.match(r"^\s*#elif.*$", line):
                # print("<debug #elif> stack:", matchFlagStack, line, end=' => ')
                if len(matchFlagStack)>=2 and matchFlagStack[-2]==0:  # avoid pre-condition (parent) is false but cur-condition true then wrongly add
                    matchFlagStack[-1] = 0
                    # print(matchFlagStack)
                    continue
                # condition interpret
                if matchFlagStack[-1] == 1:
                    matchFlagStack[-1] = 0
                else:
                    condition = re.match(r"^\s*#elif\s+(.*?)\s*$", line).group(1)
                    condition = self._rmBoundaryBlank(condition)
                    condition = self._splitPostComment(condition)[0]
                    cond = self._macroConditionInterpret(condition, objmacro_dict, funcmacro_dict)
                    if cond:
                        matchFlagStack[-1] = 1
                        # if len(matchFlagStack) <= 1:
                        #     matchFlagStack[-1] = 1
                        # elif matchFlagStack[-2] == 1:
                        #     matchFlagStack[-1] = 1
                        # elif matchFlagStack[-2] == 0:
                        #     matchFlagStack[-1] = 0
                        # else:
                        #     pass
                # print(matchFlagStack)
                continue
            # match '#else'
            elif re.match(r"^\s*#else.*$", line):
                # print("<debug #else> stack:", matchFlagStack, line, end=' => ')
                if len(matchFlagStack)>=2 and matchFlagStack[-2]==0:
                    matchFlagStack[-1] = 0
                    # print(matchFlagStack)
                    continue
                if matchFlagStack[-1] == 1:
                    matchFlagStack[-1] = 0
                else:
                    matchFlagStack[-1] = 1
                # print(matchFlagStack)
                continue
            # match '#endif'
            elif re.match(r"^\s*#endif.*$", line):
                matchFlagStack.pop()
                continue
            else:
                pass
            # generate new line list according to interpreted result
            if len(matchFlagStack) <= 0:
                self._macroDefineMatch(nline, objmacro_dict, funcmacro_dict)
                rnlines.append(nline)
            else:
                if matchFlagStack[-1] == 1:
                    self._macroDefineMatch(nline, objmacro_dict, funcmacro_dict)
                    rnlines.append(nline)
                else:
                    pass
        return rnlines

if __name__ == "__main__":
    curdir = os.path.dirname(os.path.realpath(__file__))

    rawlines = []
    filename = 'test'
    includedir = os.path.join(curdir, '../test/include')
    filepath = os.path.join(curdir, '../test/include/general.h')
    proc = CProcessor(rawlines, filename, includedir, logmask=True)
    proc.debug_print("hello world")
    objmacro_dict, funcmacro_dict = proc.RecurseMacroParse(filepath, includedir)
    for key in objmacro_dict:
        print("<define obj> %s: %s" % (key, objmacro_dict[key]))
    # for key in funcmacro_dict:
    #     print("<define func> %s: %s" % (key, funcmacro_dict[key]))

    proc._objmacroSelfExpand(objmacro_dict)
    for key in objmacro_dict:
        print("<define obj> %s: %s" % (key, objmacro_dict[key]))

    # integrate test
    cfilepath = os.path.join(curdir, '../test/ram/hdr_ram.c')
    includedir = os.path.join(curdir, '../test/include')
    fin = open(cfilepath, 'r')
    rawlines = fin.readlines()
    rawnlines = [[i+1,line] for i,line in enumerate(rawlines)]
    fin.close()
    # connect line break
    nlines_ret = proc.LineBreakConnect(rawnlines)
    # filter specific lines
    nlines_ret = proc.LineFilter(nlines_ret, filterout=proc.FilterOutRef)
    # # parse macro
    # objmacro_dict, funcmacro_dict = proc.RecurseMacroParse(cfilepath, includedir, selfcontain=False)
    # # expand conditional macro
    # nlines_ret = proc.ConditionMacroExpand(nlines_ret, objmacro_dict, funcmacro_dict)
    # # expand intra macro
    # nlines_ret = proc.IntraMacroExpand(nlines_ret, objmacro_dict, funcmacro_dict)
    for linenum, line in nlines_ret:
        print('[line %d] %s' % (linenum, repr(line)))
    destdir = os.path.join(curdir, '../test')
    destfile = os.path.join(destdir, 'test_processor.h')
    fout = open(destfile, 'w')
    fout.writelines([i[1] for i in nlines_ret])
    fout.close()

    # line macro expand test
    linenum = 281
    line = '\tu16 pGlobalLogRatio[__FW_MAX_EXPO_NUM_-1]; //0xc6,rw=0, r=[0,16384], v={8192...}, d="exposure ratio of neighbour exposure images"\n'
    nline = proc._lineMacroExpand([linenum, line], objmacro_dict, funcmacro_dict)
    print("[line %d] %s" % (nline[0], repr(nline[1])))
