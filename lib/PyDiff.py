#-*- coding: utf-8 -*-
'''
@File       : PyDiff.py
@Date       : 2020/9/19
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependency : Python 2.7 or Python 3+
@Description: code diff lib
'''

import sys, os, re
import difflib

def PathSimplify(path, precnt=2, postcnt=1):
    '''simplify path, case like '\\10.6.19.66\dir1\subdir1\test.txt' to '\\10.6.19.66\...\test.txt' '''
    tmplist_all = re.split(r'([\\/]+)', path)
    tmplist     = re.split(r'(?:\\+|/+)', path)
    if len(tmplist) <= (precnt+postcnt):
        return path
    prelist = []
    postlist = []
    prenum = postnum = 0
    for item in tmplist_all:
        if not re.match(r"^\s*([\\/])+\s*$", item):
            prenum = prenum+1 if item else prenum
            # print("<debug>", item)
        if prenum <= precnt:
            prelist.append(item)
        else:
            break
    # print("<debug>", prelist)
    for item in tmplist_all[::-1]:
        if not re.match(r"^\s*[\\/]+\s*$", item):
            postnum = postnum+1 if item else postnum
        if postnum <= postcnt:
            postlist.insert(0, item)
        else:
            break
    totalist = prelist + ['...'] + postlist
    return ''.join(totalist)

class FileDiff:
    def __init__(self):
        pass
    
    @classmethod
    def _readlines(cls, filepath):
        # with open(filepath, 'rb') as fbin:
        #     content = fbin.read().decode('utf-8')
        #     lines = content.splitlines()
        with open(filepath, 'r') as fin:
            lines = fin.readlines()
        return lines
    
    @classmethod
    def _read(cls, filepath):
        with open(filepath, 'r') as fin:
            content = fin. read()
        return content

    @classmethod
    def compare(cls, filecmp1, filecmp2, destdir, destname="result"):
        # check input param
        if not os.path.exists(filecmp1):
            ErrorInfo = "invalid file for cmp1 '%s'" % filecmp1
            raise RuntimeError(ErrorInfo)
        if not os.path.exists(filecmp2):
            ErrorInfo = "invalid file for cmp2 '%s'" % filecmp2
            raise RuntimeError(ErrorInfo)
        if not os.path.exists(destdir):
            WarningInfo = "dest directory '%s' does not exist and create it"
            print(WarningInfo)
            os.makedirs(destdir)
        # generate destname
        destfilename = destname
        shotname, extension = os.path.splitext(destname)
        if extension != '.html':
            destfilename = shotname + '.html'
        destfile = os.path.join(destdir, destfilename)
        # compare
        lines1 = cls._readlines(filecmp1)
        lines2 = cls._readlines(filecmp2)
        comp   = difflib.HtmlDiff()
        result = comp.make_file(lines1, lines2, fromdesc=filecmp1, todesc=filecmp2, context=False)
        # result = comp.make_table(lines1, lines2)
        # write to destfile
        with open(destfile, 'w') as fout:
            fout.writelines(result)
        return destfile

    @classmethod
    def comparelines(cls, linescmp1, linescmp2, destdir, desc1='', desc2='', destname="result"):
        if not os.path.exists(destdir):
            os.makedirs(destdir)
        # generate destname
        destfilename = destname
        shotname, extension = os.path.splitext(destname)
        if extension != '.html':
            destfilename = shotname + '.html'
        destfile = os.path.join(destdir, destfilename)
        # compare
        comp   = difflib.HtmlDiff()
        result = comp.make_file(linescmp1, linescmp2, fromdesc=desc1, todesc=desc2)
        # dump
        with open(destfile, 'w') as fout:
            fout.writelines(result)
        return destfile

    @classmethod
    def similarity(cls, filecmp1, filecmp2, precision=5):
        # check input param
        if not os.path.exists(filecmp1):
            ErrorInfo = "invalid file for cmp1 '%s'" % filecmp1
            raise RuntimeError(ErrorInfo)
        if not os.path.exists(filecmp2):
            ErrorInfo = "invalid file for cmp2 '%s'" % filecmp2
            raise RuntimeError(ErrorInfo)
        content1 = cls._read(filecmp1)
        content2 = cls._read(filecmp2)
        seq = difflib.SequenceMatcher(lambda i: True if re.match(r"\s+", i) else False, content1, content2)
        # seq = difflib.SequenceMatcher(lambda i: i in " \t\r\n", content1, content2)
        return round(seq.ratio(), precision)
    
    @classmethod
    def similaritycontent(cls, content1, content2, precision=5):
        seq = difflib.SequenceMatcher(lambda i: True if re.match(r"\s+", i) else False, content1, content2)
        return round(seq.ratio(), precision)

if __name__ == "__main__":
    curdir = os.path.dirname(os.path.realpath(__file__))