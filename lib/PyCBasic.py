#-*- coding: utf-8 -*-
'''
@File   : PyCBasic.py
@Data   : 2023/7/21
@Author : Kevin Peng
@Version: 0.1
@License: MIT License
@Desc:  : C basic class including some basic proc functions 
'''

import sys,os,re

BitSize8List= ['u8', 's8', 'char', 'unsigned char', 'signed char']
BitSize16List= ['u16', 's16', 'short', 'unsigned short', 'signed short']
BitSize32List= ['u32', 's32', 'int', 'unsigned int', 'signed int']
BitSize64List= ['u64', 's64', 'long long', 'unsigned long long', 'signed long long']

TYPELIST= BitSize8List + BitSize16List + BitSize32List + BitSize64List

class CBasic(Object):
    def __init__(self):
        super().__init__()
    
    def _deStringConstant(self, inString, lineIndex=None):
        '''remove string constant'''
        retString = ''
        quotaionMarkFlag = False
        for char in inString:
            # if char == '\"' or char == '\'':    # char is " or '
            if char == '\"':    # char is "
                quotaionMarkFlag = not quotaionMarkFlag
                continue
            if not quotaionMarkFlag:
                retString = retString + char
        if quotaionMarkFlag:
            ErrorInfo = "[Line %d] Error in _deStringConstant: invalid input string \'%s\'." % (lineIndex, inString) if lineIndex else \
                                  "Error in _deStringConstant: invalid input string \'%s\'." % inString
            raise RuntimeError(ErrorInfo)
        return retString

    def _splitStringConstant(self, inString, lineIndex=None):
        '''remove string constant'''
        retString = ''
        constString = ''
        quotaionMarkFlag = False
        for char in inString:
            # if char == '\"' or char == '\'':    # char is " or '
            if char == '\"':    # char is "
                quotaionMarkFlag = not quotaionMarkFlag
                continue
            if not quotaionMarkFlag:
                retString = retString + char
            else:
                constString = constString + char
        if quotaionMarkFlag:
            ErrorInfo = "[Line %d] Error in _deStringConstant: invalid input string \'%s\'." % (lineIndex, inString) if lineIndex else \
                                  "Error in _deStringConstant: invalid input string \'%s\'." % inString
            raise RuntimeError(ErrorInfo)
        return retString, constString

    def _simplify(self, inString, postfixList=['u', 'ul'], typeList=TYPELIST):
        '''simplify input expression, including: remove type cast, remove postfix after num const'''
        tmpString = self._deTypeCast(inString, typeList=typeList)
        return ''.join([i if not re.match(r'^\s*(?:0x[0-9a-f]+|\d+)(?:u|ul)?\s*$', i, re.I) else self._dePostfix(i, postfixList=postfixList) for i in re.split(r'\s*(\w+)\s*', tmpString)])
    
    def _dePostfix(self, inString, postfixList=['u', 'ul']):
        '''remove postfix from input string'''
        postfix = '|'.join([self._rmBoundaryBlank(i) for i in postfixList])
        pattern = r'^(.*?)(?:%s)\s*$' % postfix
        match_dePostfix = re.match(pattern, inString, re.I)
        return  match_dePostfix.group(1) if match_dePostfix else inString
    
    def _deTypeCast(self, exprString, typeList=TYPELIST):
        '''remove type cast as well as caused empty ()'''
        tmpList = [i for i in re.split(r'(\w+)', exprString) if i not in typeList]
        tmpString = ''.join(tmpList)
        retString = ''.join(re.split(r'\s*\(\s*\)\s*', tmpString))
        return retString if len(re.findall(r'\w+', retString))>1 else ''.join(re.findall(r'\w+', retString))

    def _rmPreKeyword(self, inString, keywords=['volatile', 'static', 'extern', 'const']):
        '''remove key word of pre-position'''
        # return ' '.join([i for i in inString.split(' ') if not i in keywords])
        return ' '.join([i for i in re.split(r'\s+', inString) if not i in keywords and len(i)>0])
    
    def _splitPostComment(self, line, splitblank=False):
        '''split in-line comment'''
        pattern_inline = r"^(.+?)(\s*)((?:\/{2,}|\/\*).*)$"
        match_inline = re.match(pattern_inline, line, re.S)  # re.S: match include '\n', important for line parse
        if splitblank == False:
            if match_inline:
                lineNoComment = match_inline.group(1)
                comment = match_inline.group(3)
            else:
                lineNoComment = self._rmBoundaryBlankR(line)
                comment = self._getBoundaryBlankR(line)
            return (lineNoComment, comment)
        else:
            if match_inline:
                lineNoComment = match_inline.group(1)
                blank = match_inline.group(2)
                comment = match_inline.group(3)
            else:
                lineNoComment = self._rmBoundaryBlankR(line)
                blank = self._getBoundaryBlankR(line)
                comment = ''
            return (lineNoComment, blank, comment)

    def _rmBoundaryBlank(self, inString):
        '''remove blank char at boundary'''
        return re.match(r"^\s*(.*?)\s*$", inString).group(1)  # ? is the key in pattern (.+?)
                                                              # ^ and $ is the key of pattern
    def _rmBoundaryBlankR(self, inString):
        '''similar to _rmBoundaryBlank, but only remove blanks on the right side'''
        return re.match(r"^(.*?)\s*$", inString, re.S).group(1)
    
    def _rmBoundaryBlankL(self, inString):
        '''similar to _rmBoundaryBlank, but only remove blanks on the left side'''
        return re.match(r"^\s*(.*)$", inString, re.S).group(1)

    def _getBoundaryBlankR(self, inString):
        '''get blank char at right boundary'''
        return re.match(r"^.*?(\s*)$", inString, re.S).group(1)

    def _getBoundaryBlankL(self, inString):
        '''get blank char at right boundary'''
        return re.match(r"^(\s*).*?$", inString).group(1)

    def _mergeBlank(self, inString, junction=' '):
        '''merge blank chars in read-in string into given junction str'''
        return junction.join(re.split(r"\s+", inString))

    def _simplifyPath(self, path, precnt=2, postcnt=1):
        '''simplify path, case like '\\10.6.19.66\dir1\subdir1\test.txt' to '\\10.6.19.66\...\test.txt' '''
        tmplist_all = re.split(r'([\\/]+)', path)
        tmplist     = re.split(r'(?:\\+|/+)', path)
        if len(tmplist) <= (precnt+postcnt):
            return path
        prelist = []
        postlist = []
        prenum = postnum = 0
        for item in tmplist_all:
            if not re.match(r"^\s*([\\/])+\s*$", item):
                prenum = prenum+1 if item else prenum
                # print("<debug>", item)
            if prenum <= precnt:
                prelist.append(item)
            else:
                break
        # print("<debug>", prelist)
        for item in tmplist_all[::-1]:
            if not re.match(r"^\s*[\\/]+\s*$", item):
                postnum = postnum+1 if item else postnum
            if postnum <= postcnt:
                postlist.insert(0, item)
            else:
                break
        totalist = prelist + ['...'] + postlist
        return ''.join(totalist)
