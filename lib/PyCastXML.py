#-*- coding: utf-8 -*-
'''
@File       : PyCastXML.py
@Date       : 2020/9/24
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependency : Python 2.7 or Python 3+
@Description: parse castxml out xml
'''
import os, sys, re
import xmltodict, json
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


if __name__ == "__main__":
    curdir = os.path.dirname(os.path.realpath(__file__))
    xmlpath = os.path.join(curdir, "../test/test_castxml.xml")
    xmlpath = os.path.realpath(xmlpath)
    print(xmlpath)

    # xml to jason test
    # f = open(xmlpath, 'r')
    # content = f.read()
    # content_parse = xmltodict.parse(content)
    # print(type(content_parse))
    # fout = open(os.path.join(curdir, "../tmp/testxmlparse.json"), 'w')
    # json_str = json.dump(content_parse, fout, indent=4)
    # fout.close()
    # f.close()
    # print("done")

    # tree = ET.ElementTree(file=xmlpath)
    tree = ET.parse(xmlpath)
    root = tree.getroot()
    # print(root)
    for elem in root.findall("Typedef[@name='TControl']"):
        print(elem.tag, elem.get("name"), elem.get("type"))
        for e in root.findall("Struct[@id='_37']"):
            print(e.get("size"))