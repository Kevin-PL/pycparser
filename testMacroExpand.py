#-*- coding: utf-8 -*-
import sys, os, re

def _rmBoundaryBlank(inString):
    '''remove blank char at boundary'''
    return re.match(r"^\s*(.*?)\s*$", inString).group(1)  # ? is the key in pattern (.+?)
                                                            # ^ and $ is the key of pattern
def _rmBoundaryBlankR(inString):
    '''similar to _rmBoundaryBlank, but only remove blanks on the right side.'''
    return re.match(r"^(.*?)\s*$", inString).group(1)

def _rmBoundaryBlank(inString):
    '''remove blank char at boundary'''
    return re.match(r"^\s*(.*?)\s*$", inString).group(1)  # ? is the key in pattern (.+?)
                                                        # ^ and $ is the key of pattern

def _mergeBlank(inString, junction=' '):
    '''merge blank chars in read-in string into given junction str'''
    return junction.join(re.split(r"\s+", inString))

def MacroDefMatch(line, macro_dict):
    ret = False
    pattern_def = r"^\s*#define\s+(\w+)(.*)$"
    match_def = re.match(pattern_def, line)
    if match_def:
        macro = match_def.group(1)
        define= match_def.group(2)
        macro_dict[macro] = define
        ret = True
    else:
        ret = False
    return ret

def MacroConditionInterpret(condition, macro_dict):
    '''interpret macro condition'''
    tmplist = re.split(r"(\W+)", condition)
    newtmplist = []
    for i in tmplist:
        if i in macro_dict:
            newtmplist.append(macro_dict[i])
        else:
            newtmplist.append(i)
    newcondition = ''.join(newtmplist)
    return bool(eval(newcondition))

def preparse(lines):
    rank_dict = {}
    rank = -1
    for linenum,rawline in enumerate(lines):
        line = _rmBoundaryBlank(rawline)
        if re.match(r"^\s*#if.*$", line):
            rank += 1
            rank_dict[linenum] = rank
        elif re.match(r"^\s*#elif.*$", line):
            rank_dict[linenum] = rank
        elif re.match(r"^\s*#else.*$", line):
            rank_dict[linenum] = rank
        elif re.match(r"^\s*#endif.*$", line):
            rank_dict[linenum] = rank
            rank -= 1
        else:
            pass
    return rank_dict

def parse(lines, macro_dict):
    newlinelist = []
    matchFlagStack = []
    for rawline in lines:
        line = _rmBoundaryBlank(rawline)
        # match '#if'
        if re.match(r"^\s*#if.*$", line):
            print("<debug #if> stack:", matchFlagStack, line, end=' => ')
            if matchFlagStack and matchFlagStack[-1] == 0:
                matchFlagStack.append(0)
                print(matchFlagStack)
                continue
            # condition interpret
            match_if = re.match(r"^\s*#if\s+(.*?)\s*$", line)
            match_ifdef = re.match(r"^\s*#ifdef\s+(\w+).*$", line)
            match_ifndef = re.match(r"^\s*#ifndef\s+(\w+).*$", line)
            if match_if:
                condition = match_if.group(1)
                condition = _rmBoundaryBlank(condition)
                cond = MacroConditionInterpret(condition, macro_dict)
            elif match_ifdef:
                condition = match_ifdef.group(1)
                cond = condition in macro_dict
            elif match_ifndef:
                condition = match_ifndef.group(1)
                cond = condition not in macro_dict
            else:
                ErrorInfo = "unvalid #if definition '%s'" % line
                raise RuntimeError(ErrorInfo)
            # process
            if cond:
                matchFlagStack.append(1)
                # if not matchFlagStack:  # empty stack
                #     matchFlagStack.append(1)
                # # avoid case of pre-condition false but cur-condition true
                # elif matchFlagStack[-1] == 1:  # pre-condition true
                #     matchFlagStack.append(1)
                # elif matchFlagStack[-1] == 0:  # pre-condition false
                #     matchFlagStack.append(0)
                # else:
                #     pass
            else:
                matchFlagStack.append(0)
            print(matchFlagStack)
            continue
        # match '#elif'
        elif re.match(r"^\s*#elif.*$", line):
            print("<debug #elif> stack:", matchFlagStack, line, end=' => ')
            if len(matchFlagStack)>=2 and matchFlagStack[-2]==0:
                matchFlagStack[-1] = 0
                print(matchFlagStack)
                continue
            # condition interpret
            if matchFlagStack[-1] == 1:
                matchFlagStack[-1] = 0
            else:
                condition = re.match(r"^\s*#elif\s+(.*?)\s*$", line).group(1)
                condition = _rmBoundaryBlank(condition)
                cond = MacroConditionInterpret(condition, macro_dict)
                if cond:
                    matchFlagStack[-1] = 1
                    # if len(matchFlagStack) <= 1:
                    #     matchFlagStack[-1] = 1
                    # elif matchFlagStack[-2] == 1:
                    #     matchFlagStack[-1] = 1
                    # elif matchFlagStack[-2] == 0:
                    #     matchFlagStack[-1] = 0
                    # else:
                    #     pass
            print(matchFlagStack)
            continue
        # match '#else'
        elif re.match(r"^\s*#else.*$", line):
            print("<debug #else> stack:", matchFlagStack, line, end=' => ')
            if len(matchFlagStack)>=2 and matchFlagStack[-2]==0:
                matchFlagStack[-1] = 0
                print(matchFlagStack)
                continue
            if matchFlagStack[-1] == 1:
                matchFlagStack[-1] = 0
            else:
                matchFlagStack[-1] = 1
            print(matchFlagStack)
            continue
        # match '#endif'
        elif re.match(r"^\s*#endif.*$", line):
            matchFlagStack.pop()
            continue
        else:
            pass
        ## generate new line list according to interpreted result
        if len(matchFlagStack) <= 0:
            MacroDefMatch(line, macro_dict)
            newlinelist.append(rawline)
        else:
            if matchFlagStack[-1] == 1:
                MacroDefMatch(line, macro_dict)
                newlinelist.append(rawline)
            else:
                pass
    return newlinelist

if __name__ == "__main__":
    curdir = os.path.dirname(os.path.realpath(__file__))
    filepath = os.path.join(curdir, "test/test_include.h")
    destfile = os.path.join(curdir, "test/test_macro_result.h")
    with open(filepath, 'r') as fin:
        lines = fin.readlines()
        # for line in lines:
        #     print(line, end='')
        macro_dict = {}
        newlinelist = parse(lines, macro_dict)
        for line in newlinelist:
            print(line, end='')
        fout = open(destfile, 'w')
        fout.writelines(newlinelist)
        fout.close()

        # rank_dict = preparse(lines)
        # for i in rank_dict:
        #     print(rank_dict[i], lines[i], end='')